package cl.ceisufro.filehelper.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.provider.DocumentFile;
import android.text.TextUtils;
import android.util.Size;
import android.webkit.MimeTypeMap;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Luis Andrés Jara Castillo on 25-09-17.
 */

public class FileHelper {
    /**
     * Crea un archivo vació en el directorio y con el nombre y extensión ingresados como parámetro.
     *
     * @param filePath  {@link String} - Directorio en el cual sera guardado el archivo, se creara si no existe.
     *                  Si no es posible crearlo no se creara el archivo y se retornara null.
     * @param fileName  {@link String} - Nombre que se le dara al archivo. Evitar ingresar el archivo con extensión
     *                  ya que sera sustituida por el parámetro extension de todas formas.
     *                  (EJ: fileName:archivo.jpg, extension:".mp3", resultado:"archivo.jpg.mp3"):
     * @param extension {@link String} - Extension que se le dara al archivo guardado.
     * @return Instancia de la clase {@link File} representando al archivo creado o null si no fue posible
     * crear el archivo.
     */
    public static File createNewEmptyFile(String filePath, String fileName, String extension) {
        try {
            File dir = new File(filePath);
            if (!dir.isDirectory()) {
                String path = getParentDirectoryFromFile(dir);
                dir = new File(path);
            }
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    return null;
                }
            }
            extension = extension.replaceAll("\\.", "");
            File file = new File(dir, (fileName + "." + extension));
            if (file.createNewFile()) {
                return file;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Crea un archivo vació en el directorio y con el nombre y tipo especificado.
     *
     * @param parent   {@link DocumentFile} - Directorio para guardar el archivo
     * @param fileName {@link String} - Nombre del archivo con extension acorde al mimeType
     * @param mimeType {@link String} - MimeType del archivo. Ej: "image/jpeg" para crear una imagen
     *                 de tipo "jpeg", la extension debería ser .jpeg o .jpg
     * @return {@link DocumentFile} . Archivo creado o null si no fue posible crearlo.
     */
    public static DocumentFile createNewEmptyFile(DocumentFile parent, String fileName, String mimeType) {
        try {
            return parent.createFile(mimeType, fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getSDCardName(Context context) {
        File sdCard = getExternalStorageDirectory(context);
        if (sdCard != null && !TextUtils.isEmpty(sdCard.getPath())) {
            String cardName = "";
            String split[] = sdCard.getPath().split(File.separator);
            cardName = split.length > 0 ? split[2] : null;
            return cardName;
        }
        return null;
    }

    /**
     * Devuelve el espacio disponible en el directorio en MegaBytes.
     */
    public static long getAvailableStorage(File directorio) {
        long bytesAvailable = 0;
        try {
            StatFs stat = new StatFs(directorio.getPath());
            bytesAvailable = stat.getAvailableBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytesAvailable;
    }

    /**
     * Entrega la extension del archivo ingresado.
     *
     * @param file {@link File} - Archivo al cual se le extraerá la extension.
     * @return {@link String} con la extension, sin punto, del archivo ingresado o null si no es posible
     * extraer la extensión ya sea por que el archivo era un directorio o este no tenia extensión.
     */
    public static String getFileExtension(File file) {
        try {
            if (file != null && !TextUtils.isEmpty(file.getPath()) && !file.isDirectory()) {
                String filename = file.getName();
                String[] split = filename.split("\\.");
                return split.length >= 2 ? split[split.length - 1] : null;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Entrega el MimeType del archivo ingresado como parámetro en formato de cadena de texto.
     * </p>
     * El formato es el siguiente para un archivo JPG: "image/jpg".
     * La parte previa al "/" es el tipo de archivo y la parte siguiente es el subtipo.
     * Por lo tanto el archivo seria una imagen de subtipo jpg.
     *
     * @param file {@link File} - Archivo para obtener el MimeType.
     * @return {@link String} MimeType del archivo.
     */
    public static String getMimeTypeAsString(File file) {
        String type = null;
        try {
            if (file != null && !TextUtils.isEmpty(file.getPath()) && !file.isDirectory()) {
                String extension = getFileExtension(file);
                if (extension != null) {
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return type;
    }

    /**
     * Entrega el MimeType del archivo ingresado como parámetro.
     *
     * @param file {@link File} - Archivo para obtener el MimeType.
     * @return {@link MimeType} MimeType del archivo.
     */
    public static MimeType getMimeTypeFromFile(File file) {
        try {
            String typeAsString = getMimeTypeAsString(file);
            if (!TextUtils.isEmpty(typeAsString)) {
                return new MimeType(typeAsString);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna el MimeType para una extension de archivo
     *
     * @param extension - extension sin punto
     * @return MimeType de un archivo
     */
    public static String getMimeTypeFromExtension(String extension) {
        String type = null;
        try {
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return type;
    }

    /**
     * retorna el mimetype de un archivo en una url
     *
     * @param urlString - url del archivo
     * @return mimetype del archivo
     */
    public static String getMimeTypeFromUrl(String urlString) {
        try {
            String extension = MimeTypeMap.getFileExtensionFromUrl(urlString);
            return getMimeTypeFromExtension(extension);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna el directorio de un archivo.
     * </p>
     * Ej: si el archivo tiene ruta "/storage/emulated/0/Music/archivo_audio.mp3", se retornara,
     * "/storage/emulated/0/Music/"
     *
     * @param file {@link File} - Archivo para obtener el directorio, si no es un archivo se retornara el parámetro.
     * @return {@link String} con el directorio del archivo.
     */
    public static String getParentDirectoryFromFile(File file) {
        if (getFileExtension(file) != null) {
            String split[] = file.getPath().split(File.separator);
            int splitLenght = split.length;
            String fileName = split[splitLenght - 1];
            return file.getPath().replace((File.separator + fileName), "");
        } else {
            return file.getPath();
        }
    }

    /**
     * Retorna la duración en segundos de un archivo de audio.
     *
     * @param context {@link Context} - Context para obtener el valor
     * @param file    {@link File} - Archivo para obtener la duración.
     * @return int con la duración del archivo de audio.
     */
    public static int getAudioDurationFromFile(Context context, File file) {
        try {
            Uri uri = Uri.parse(file.getPath());
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(context, uri);
            String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int millSecond = Integer.parseInt(durationStr);
            return (int) TimeUnit.MILLISECONDS.toSeconds(millSecond);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Obtiene directamente el directorio para archivos del usuario.
     * Se encarga de detectar que sea el que existe en la memoria interna.
     *
     * @param context {@link Context} - Contexto para obtener la informacion
     * @return Directorio interno de la aplicación
     */
    public static File getInternalStorageDirectory(Context context) {
        return context.getExternalFilesDir(null);

    }

    /**
     * Obtiene el directorio de archivos de la tarjeta SD si existe una
     * sino devuelve un directorio nulo.
     *
     * @return File con la dirección de archivos de la tarjeta SD o
     * null si no existe el directorio o la tarjeta no existe.
     */
    public static File getExternalStorageDirectory(Context context) {
        if (isExternalStorageMounted()) {
            File dirs[] = context.getExternalFilesDirs(null);
            for (File dir : dirs) {
                if (dir != null &&
                        !dir.getAbsolutePath().contains("emulated")) {
                    return dir;
                }
            }
        }
        return null;
    }

    /**
     * Retorna el directorio princiapl de la memoria interna
     *
     * @param context
     * @return File del directorio o null si se produce un error
     */
    public static File getInternalMainStorageDirectory(Context context) {
        try {
            File file = getInternalStorageDirectory(context);
            String path = file.getPath();
            String split[] = path.split("/");
            path = File.separator + split[1] + File.separator + split[2] + File.separator + split[3];
            return new File(path);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna el directorio princiapl de la memoria externa
     *
     * @param context
     * @return File del directorio o null si no se encuentra
     */
    public static File getExternalMainStorageDirectory(Context context) {
        try {
            File file = getExternalStorageDirectory(context);
            String path = file.getPath();
            String split[] = path.split("/");
            path = File.separator + split[1] + File.separator + split[2];
            return new File(path);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Comprueba que esta la tarjeta SD montada en el sistema
     *
     * @return true si la tarjeta esta montada; false si no esta montada.
     */
    public static boolean isExternalStorageMounted() {
        //if (Environment.isExternalStorageRemovable()) {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED) || state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        /*} else {
            return false;
        }*/
    }

    public static boolean isExternalStorageReadOnly() {
        if (Environment.isExternalStorageRemovable()) {
            String state = Environment.getExternalStorageState();
            return state.equals(Environment.MEDIA_MOUNTED_READ_ONLY);
        } else {
            return false;
        }
    }

    /**
     * copia un archivo o un directorio con la totalidad su contenido hacia el
     * directorio de destino.
     *
     * @param fileOrDir {@link String} - Archivo/Directorio para copiar.
     * @param dstDir    {@link String} - Directorio de destino.
     */
    public static void copyRecursively(String fileOrDir, String dstDir) {
        try {
            File src = new File(fileOrDir);
            File dst = new File(dstDir, src.getName());
            if (src.isDirectory()) {

                String files[] = src.list();
                for (String file : files) {
                    String src1 = (new File(src, file).getPath());
                    String dst1 = dst.getPath();
                    copyRecursively(src1, dst1);
                }
            } else {
                copyFile(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Copia un archivo hacia otro directorio.
     *
     * @param sourceFile {@link File} - Archivo de origen para copiar.
     * @param destFile   {@link File} - Archivo de destino hacia el cual copiar.
     */
    public static void copyFile(File sourceFile, File destFile) {
        try {
            if (!destFile.getParentFile().exists())
                destFile.getParentFile().mkdirs();

            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            FileChannel source = null;
            FileChannel destination = null;
            try {
                source = new FileInputStream(sourceFile).getChannel();
                destination = new FileOutputStream(destFile).getChannel();
                destination.transferFrom(source, 0, source.size());
            } finally {
                if (source != null) {
                    source.close();
                }
                if (destination != null) {
                    destination.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeRecursively(File fileOrDir) {
        try {
            if (fileOrDir.exists()) {
                if (fileOrDir.isDirectory() && fileOrDir.listFiles().length > 0) {
                    File[] files = fileOrDir.listFiles();
                    for (File file : files) {
                        if (file.isDirectory()) {
                            removeRecursively(fileOrDir);
                        } else {
                            file.delete();
                        }
                    }
                } else {
                    fileOrDir.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Escribe los datos de un Bitmap en un archivo.
     *
     * @param destino {@link File} - Archivo en el cual iran los datos del Bitmap
     * @param bitmap  {@link Bitmap} - Bitmap que sera escrito en el archivo.
     * @return boolean - true si el bitmap es guardado con éxito en el archivo; false
     * si algun problema se presenta.
     */
    public static boolean writeBitmapIntoFile(File destino, Bitmap bitmap) {
        FileOutputStream out = null;
        String dirPath = destino.getPath().replace(destino.getName(), "");
        File dir = new File(dirPath);
        try {
            if (!destino.exists()) {
                if (!dir.exists()) {
                    if (dir.mkdirs()) {
                        if (!destino.createNewFile()) {
                            return false;
                        }
                    }
                }
            }
            try {
                out = new FileOutputStream(destino);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Obtiene una instancia de Bitmap de un archivo de imagen.
     *
     * @param file {@link File} - Archivo de imagen para extraer el bitmap.
     * @return {@link Bitmap} decodificado del archivo o null si el archivo no es una imagen o no existe bitmap que obtener.
     */
    public static Bitmap getBitmapFromFile(File file) {
        try {
            return BitmapFactory.decodeFile(file.getPath());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna la miniatura de un archivo de video en una instancia de Bitmap.
     *
     * @param file {@link File} - Archivo de video para obtener la miniatura.
     * @return {@link Bitmap} que tiene la minuatura del video.
     */
    public static Bitmap getVideoThumbnail(File file) {
        try {
            return ThumbnailUtils.createVideoThumbnail(file.getPath(),
                    MediaStore.Images.Thumbnails.MINI_KIND);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna la miniatura de un archivo de imagen en una instancia de Bitmap.
     *
     * @param file {@link File} - Archivo de imagen para obtener la miniatura.
     * @return {@link Bitmap} que tiene la miniatura del imagen.
     */
    public static Bitmap getImageThumbnail(File file) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getPath(), options);
            Size dimesionesOptimas = getOptimalDimentions(options.outWidth, options.outHeight, 100, 100);
            return ThumbnailUtils.extractThumbnail(getBitmapFromFile(file),
                    dimesionesOptimas.getWidth(), dimesionesOptimas.getHeight());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna las dimensiones óptimas para una imagen a partir de sus dimensiones originales y las
     * finales, respetando la relación de aspecto del archivo original.
     *
     * @param bitmap      {@link Bitmap} - Bitmap de la imagen para obtener sus dimensiones originales.
     * @param targetWidth int - Ancho objetivo de la imagen.
     * @param targetHeigh int - Alto objetivo de la imagen.
     * @return Instancia de {@link Size} con las dimensiones óptimas para el archivo.
     */
    public static Size getOptimalDimentions(Bitmap bitmap, int targetWidth, int targetHeigh) {
        Size outputSize;
        float targetRatio = (float) targetWidth / targetHeigh;
        float sourceRatio = (float) bitmap.getWidth() / bitmap.getHeight();
        int optimalWidth, optimalHeight;
        if (sourceRatio >= targetRatio) {
            optimalWidth = targetWidth;
            optimalHeight = (int) (optimalWidth / sourceRatio);
        } else {
            optimalHeight = targetHeigh;
            optimalWidth = (int) (optimalHeight * sourceRatio);
        }
        outputSize = new Size(optimalWidth, optimalHeight);
        return outputSize;
    }

    /**
     * Retorna las dimensiones óptimas para una imagen a partir de sus dimensiones originales y las
     * finales, respetando la relación de aspecto del archivo original.
     *
     * @param bitmapWidth  int - Ancho original de la imagen.
     * @param bitmapHeight int - Alto original de la imagen.
     * @param targetWidth  int - Ancho objetivo de la imagen.
     * @param targetHeigh  int - Alto objetivo de la imagen.
     * @return Instancia de {@link Size} con las dimensiones óptimas para el archivo.
     */
    public static Size getOptimalDimentions(int bitmapWidth, int bitmapHeight, int targetWidth, int targetHeigh) {
        Size outputSize;
        float targetRatio = (float) targetWidth / targetHeigh;
        float sourceRatio = (float) bitmapWidth / bitmapHeight;
        int optimalWidth, optimalHeight;
        if (sourceRatio >= targetRatio) {
            optimalWidth = targetWidth;
            optimalHeight = (int) (optimalWidth / sourceRatio);
        } else {
            optimalHeight = targetHeigh;
            optimalWidth = (int) (optimalHeight * sourceRatio);
        }
        outputSize = new Size(optimalWidth, optimalHeight);
        return outputSize;
    }

    /**
     * Comprime un directorio con sus archivos
     *
     * @param destFile carpeta de destino. Si es un archivo se creara un .zip utilizando el nombre del archivo
     * @param filelist listado de archivos
     * @return ubicacion del archivo zip o null si no se pudo crear
     */
    public static File zipFiles(File destFile, File[] filelist) {
        try {
            if (filelist != null && filelist.length > 0) {
                BufferedInputStream origin;
                if (destFile.isDirectory() || !Objects.equals(getFileExtension(destFile), "zip")) {
                    destFile = new File(destFile, (destFile.getName() + ".zip"));
                }
                FileOutputStream dest = new FileOutputStream(destFile);
                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
                int buffer = 2048;
                byte data[] = new byte[buffer];
                for (File folderFile : filelist) {
                    if (folderFile.exists()) {
                        FileInputStream fi = new FileInputStream(folderFile);
                        origin = new BufferedInputStream(fi, buffer);

                        ZipEntry entry = new ZipEntry(folderFile.getName());
                        out.putNextEntry(entry);
                        int count;

                        while ((count = origin.read(data, 0, buffer)) != -1) {
                            out.write(data, 0, count);
                        }
                        origin.close();
                    }
                }
                out.close();
                return destFile;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Comprime un directorio con sus archivos
     *
     * @param destFile carpeta de destino. Si es un archivo se creara un .zip utilizando el nombre del archivo
     * @param filelist listado de archivos
     * @return ubicacion del archivo zip o null si no se pudo crear
     */
    public static File zipFiles(File destFile, ArrayList<File> filelist) {
        try {
            if (filelist != null && !filelist.isEmpty()) {
                BufferedInputStream origin;
                if (destFile.isDirectory() || !Objects.equals(getFileExtension(destFile), "zip")) {
                    destFile = new File(destFile, (destFile.getName() + ".zip"));
                }
                FileOutputStream dest = new FileOutputStream(destFile);
                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
                int buffer = 2048;
                byte data[] = new byte[buffer];
                for (File folderFile : filelist) {
                    if (folderFile.exists()) {
                        FileInputStream fi = new FileInputStream(folderFile);
                        origin = new BufferedInputStream(fi, buffer);

                        ZipEntry entry = new ZipEntry(folderFile.getName());
                        out.putNextEntry(entry);
                        int count;

                        while ((count = origin.read(data, 0, buffer)) != -1) {
                            out.write(data, 0, count);
                        }
                        origin.close();
                    }
                }
                out.close();
                return destFile;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Comprime un directorio con sus archivos
     *
     * @param destFile carpeta de destino. Si es un archivo se creara un .zip utilizando el nombre del archivo
     * @param filelist listado de archivos
     * @return ubicacion del archivo zip o null si no se pudo crear
     */
    public static File zipFilesFromPaths(File destFile, ArrayList<String> filelist) {
        try {
            if (filelist != null && !filelist.isEmpty()) {
                BufferedInputStream origin;
                if (destFile.isDirectory() || !Objects.equals(getFileExtension(destFile), "zip")) {
                    destFile = new File(destFile, (destFile.getName() + ".zip"));
                }
                FileOutputStream dest = new FileOutputStream(destFile);
                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
                int buffer = 2048;
                byte data[] = new byte[buffer];
                for (String filePath : filelist) {
                    File folderFile = new File(filePath);
                    if (folderFile.exists()) {
                        FileInputStream fi = new FileInputStream(folderFile);
                        origin = new BufferedInputStream(fi, buffer);

                        ZipEntry entry = new ZipEntry(folderFile.getName());
                        out.putNextEntry(entry);
                        int count;

                        while ((count = origin.read(data, 0, buffer)) != -1) {
                            out.write(data, 0, count);
                        }
                        origin.close();
                    }
                }
                out.close();
                return destFile;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Comprime un directorio con sus archivos
     *
     * @param destFile carpeta de destino. Si es un archivo se creara un .zip utilizando el nombre del archivo
     * @param filelist listado de archivos
     * @return ubicacion del archivo zip o null si no se pudo crear
     */
    public static File zipFilesFromPaths(File destFile, String[] filelist) {
        try {
            if (filelist != null && filelist.length > 0) {
                BufferedInputStream origin;
                if (destFile.isDirectory() || !Objects.equals(getFileExtension(destFile), "zip")) {
                    destFile = new File(destFile, (destFile.getName() + ".zip"));
                }
                FileOutputStream dest = new FileOutputStream(destFile);
                ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
                int buffer = 2048;
                byte data[] = new byte[buffer];
                for (String filePath : filelist) {
                    File folderFile = new File(filePath);
                    if (folderFile.exists()) {
                        FileInputStream fi = new FileInputStream(folderFile);
                        origin = new BufferedInputStream(fi, buffer);

                        ZipEntry entry = new ZipEntry(folderFile.getName());
                        out.putNextEntry(entry);
                        int count;

                        while ((count = origin.read(data, 0, buffer)) != -1) {
                            out.write(data, 0, count);
                        }
                        origin.close();
                    }
                }
                out.close();
                return destFile;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
