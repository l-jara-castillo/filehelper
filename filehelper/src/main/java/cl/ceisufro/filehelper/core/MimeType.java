package cl.ceisufro.filehelper.core;

import java.util.Objects;

/**
 * Created by Luis Andrés Jara Castillo on 13-10-17.
 */

public class MimeType {
    private String type;
    private String subtype;

    /**
     * Constructor de la clase.
     *
     * @param type    {@link String} - Tipo de archivo. Ej: "image", "audio", etc.
     * @param subtype {@link String} - Subtipo de archivo. Ej: "mpeg", "jpeg", etc.
     */
    public MimeType(String type, String subtype) {
        this.type = type;
        this.subtype = subtype;
    }

    /**
     * Constructor de la clase. Extrae el tipo y subtipo de un string con, o que contenga,
     * el siguiente formato: "tipo/subtipo" ej: "image/jpeg".
     *
     * @param mediaType {@link String} - Cadena de texto con el formato especificado.
     */
    public MimeType(String mediaType) {
        if (mediaType != null) {
            String split[] = mediaType.split("/");
            this.type = split.length > 0 ? split[0].trim() : "";
            this.subtype = split.length > 0 ? split[1].trim() : "";
            if (this.subtype.contains(";")) {
                split = this.subtype.split(";");
                this.subtype = split.length > 0 ? split[0] : "";
            } else if (this.subtype.contains("charset=")) {
                split = this.subtype.split("charset=");
                this.subtype = split.length > 0 ? split[0] : "";
            }
        }
    }

    /**
     * Retorna el tipo de archivo. Ej: "image", "audio", etc.
     *
     * @return {@link String} con el tipo de archivo.
     */
    public String getType() {
        return type;
    }

    /**
     * Retorna el subtipo de archivo. Ej: "mpeg", "jpeg", etc.
     *
     * @return {@link String} con el tipo de archivo.
     */
    public String getSubtype() {
        return subtype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MimeType)) return false;
        MimeType mimeType = (MimeType) o;
        return Objects.equals(getType(), mimeType.getType()) &&
                Objects.equals(getSubtype(), mimeType.getSubtype());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getSubtype());
    }

    @Override
    public String toString() {
        return "MimeType{" +
                "type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                '}';
    }
}
