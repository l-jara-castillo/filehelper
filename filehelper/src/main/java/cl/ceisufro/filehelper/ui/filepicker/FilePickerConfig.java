package cl.ceisufro.filehelper.ui.filepicker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Creado por Luis Andrés Jara Castillo on 26-09-17.
 * <br>
 * Clase cuya finalidad es la de aplicar serialización y entregada a través
 * de un intent a la actividad {@link FilePickerActivity}.
 * La principal razón de la existencia de esta clase es debido a que no es posible
 * serializar la clase {@link FilePicker} por que este contiene como atributo una instancia
 * de {@link android.app.Activity} o {@link android.support.v4.app.Fragment} los cuales
 * no son serializables.
 */

class FilePickerConfig implements Serializable {
    private int maxFiles;
    private boolean single;
    private boolean requestPermissions;
    private String path;
    private String actionBarTitle;
    private ArrayList<String> mimeTypes;
    private ArrayList<String> mimeSubTypes;

    /**
     * Constructor de la clase.
     *
     * @param filePicker {@link FilePicker} - Instancia de {@link FilePicker}.
     */
    FilePickerConfig(FilePicker filePicker) {
        this.single = filePicker.isSingle();
        this.requestPermissions = filePicker.isRequestingPermission();
        this.maxFiles = filePicker.getMaxFiles();
        this.path = filePicker.getPath();
        this.actionBarTitle = filePicker.getActionBarTitle();
        this.mimeTypes = filePicker.getMimeTypes();
        this.mimeSubTypes = filePicker.getMimeSubTypes();
    }

    /**
     * Retorna la cantidad de archivos que se pueden seleccionar. El
     * Selector automáticamente prohibirá seleccionar mas archivos que
     * lo este valor indique.
     *
     * @return int - Cantidad de archivo que esta permitido seleccionar.
     */
    int getMaxFiles() {
        return maxFiles;
    }

    /**
     * Retorna si esta en modo de un archivo o de multiples archivos.
     *
     * @return boolean - true si esta en modo de un archivo o false si es modo de multiples archivos.
     */
    boolean isSingle() {
        return single;
    }

    /**
     * Retornar si se ha establecido que la biblioteca pida permisos de lectura de archivos
     * cuando sea lanzada.
     *
     * @return true si se ha establecido esta funcionalidad o false si no se ha establecido. Si
     * no se ha establecido se debe considerar que la aplicación los pida antes de lanzar el
     * Selector para evitar problemas.
     */
    public boolean isRequestingPermission() {
        return requestPermissions;
    }

    /**
     * Retorna la ruta desde la cual se lanzara el Selector de archivos.
     *
     * @return {@link String} - Ruta inicial para el Selector.
     */
    String getPath() {
        return path;
    }

    /**
     * Retorna los tipos de archivos permitidos para seleccionar.
     *
     * @return {@link ArrayList}<{@link String}> - Lista de tipos de archivos que se mostraran.
     */
    ArrayList<String> getMimeTypes() {
        return mimeTypes;
    }

    /**
     * Retorna los subtipos de archivos permitidos para seleccionar.
     *
     * @return {@link ArrayList}<{@link String}> - Lista de subtipos de archivos que se mostraran.
     */
    ArrayList<String> getMimeSubTypes() {
        return mimeSubTypes;
    }

    /**
     * Retorna el titulo que se mostrara en la AppBar de la aplicación.
     *
     * @return {@link String} - Titulo de la barra.
     */
    public String getActionBarTitle() {
        return actionBarTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FilePickerConfig)) return false;
        FilePickerConfig that = (FilePickerConfig) o;
        return getMaxFiles() == that.getMaxFiles() &&
                isSingle() == that.isSingle() &&
                isRequestingPermission() == that.isRequestingPermission() &&
                Objects.equals(getPath(), that.getPath()) &&
                Objects.equals(getActionBarTitle(), that.getActionBarTitle()) &&
                Objects.equals(getMimeTypes(), that.getMimeTypes()) &&
                Objects.equals(getMimeSubTypes(), that.getMimeSubTypes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMaxFiles(), isSingle(), isRequestingPermission(), getPath(), getActionBarTitle(), getMimeTypes(), getMimeSubTypes());
    }

    @Override
    public String toString() {
        return "FilePickerConfig{" +
                "maxFiles=" + maxFiles +
                ", setSingleRecordingMode=" + single +
                ", requestPermissions=" + requestPermissions +
                ", path='" + path + '\'' +
                ", actionBarTitle='" + actionBarTitle + '\'' +
                ", mimeTypes=" + mimeTypes +
                ", mimeSubTypes=" + mimeSubTypes +
                '}';
    }
}
