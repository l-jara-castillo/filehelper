package cl.ceisufro.filehelper.ui.filepicker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import cl.ceisufro.filehelper.R;
import cl.ceisufro.filehelper.core.FileHelper;
import cl.ceisufro.filehelper.core.MimeType;

/**
 * Creado por Luis Andrés Jara Castillo on 12-10-17.
 * <p>
 * Clase Adapter para generar la vista de cada elemento que tendra la lista
 * de archivos.
 */

class GridViewAdapter extends ArrayAdapter<FileExtended> {
    private ArrayList<FileExtended> originalItems = new ArrayList<>();

    /**
     * Constructor del adapter.
     *
     * @param context {@link Context} - Context para generar los elementos visuales de la lista.
     * @param objects {@link ArrayList}<{@link FileExtended}> - Lista de archivos para rellenar la lista.
     */
    GridViewAdapter(@NonNull Context context, @NonNull ArrayList<FileExtended> objects) {
        super(context, 0, objects);
        this.originalItems.addAll(objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            row = inflater.inflate(R.layout.file_picker_item_layout, parent, false);
            holder = new ViewHolder();
            holder.imageTitle = row.findViewById(R.id.file_picker_item_textview);
            holder.imageView = row.findViewById(R.id.file_picker_item_imageview);
            holder.overlay = row.findViewById(R.id.file_picker_item_overlay);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        FileExtended fileExtended = getItem(position);
        Bitmap bitmap;
        if (fileExtended != null) {
            if (fileExtended.isSelected()) {
                holder.overlay.setVisibility(View.VISIBLE);
            } else {
                holder.overlay.setVisibility(View.GONE);
            }
            if (!fileExtended.isDirectory()) {
                holder.imageTitle.setText(fileExtended.getName());
                MimeType mimeType = FileHelper.getMimeTypeFromFile(fileExtended);
                String type = "";
                if (mimeType != null && !TextUtils.isEmpty(mimeType.getType()))
                    type = mimeType.getType();
                switch (type) {
                    case "image":
                        Glide.with(getContext()).load(fileExtended).into(holder.imageView);
                        break;
                    case "video":
                        bitmap = FileHelper.getVideoThumbnail(fileExtended);
                        Glide.with(getContext()).load(bitmap).into(holder.imageView);
                        break;
                    case "text":
                        Glide.with(getContext()).load(R.drawable.ic_document).into(holder.imageView);
                        break;
                    case "audio":
                        Glide.with(getContext()).load(R.drawable.ic_audio_file).into(holder.imageView);
                        break;
                    default:
                        Glide.with(getContext()).load(R.drawable.ic_unknown_file).into(holder.imageView);
                        break;
                }
            } else {
                if (fileExtended.isPreviousDir() && position == 0) {
                    holder.imageTitle.setText("..");
                } else {
                    holder.imageTitle.setText(fileExtended.getName());
                }
                Glide.with(getContext()).load(R.drawable.ic_folder).into(holder.imageView);
            }
        }
        return row;
    }

    /**
     * Filtra los resultados de la lista según el parámetro ingresado.
     *
     * @param charText {@link String} - Texto para filtrar la lista.
     */
    void filter(String charText) {
        clear();
        if (charText.length() == 0) {
            addAll(originalItems);
        } else {
            for (FileExtended fileExtended : originalItems) {
                if (fileExtended.getName().toLowerCase().contains(charText.toLowerCase())) {
                    add(fileExtended);
                }
            }
        }
        notifyDataSetChanged();
    }

    ArrayList<FileExtended> getItems(){
        return originalItems;
    }
    /**
     * ViewHolder usado para mejorar la eficiencia de la muestra de la lista.
     */
    private static class ViewHolder {
        TextView imageTitle;
        ImageView imageView;
        ImageView overlay;
    }
}
