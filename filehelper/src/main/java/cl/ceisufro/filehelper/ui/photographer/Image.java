package cl.ceisufro.filehelper.ui.photographer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

/**
 * Creado por Luis Andrés Jara Castillo on 10-10-17.
 * <p>
 * Clase publica para retornar los resultados de las imágenes capturadas.
 */
public class Image implements Serializable {
    private String path;
    private String name;

    /**
     * Constructor de la clase
     *
     * @param path {@link String} - Ruta del archivo.
     * @param name {@link String} - Nombre de la imagen.
     */
    public Image(String path, String name) {
        this.path = path;
        this.name = name;
    }

    /**
     * Retorna la ruta de la imagen.
     *
     * @return {@link String} - Ruta de la imagen.
     */
    public String getPath() {
        return path;
    }

    /**
     * Retorna el nombre del archivo.
     *
     * @return {@link String} - Nombre del archivo.
     */
    public String getName() {
        return name;
    }

    /**
     * Retorna la imagen como un {@link Bitmap}.
     *
     * @return {@link Bitmap} - Bitmap creado a partir de la imagen. Si ocurre algún
     * error se retornara null.
     */
    public Bitmap getAsBitmap() {
        Bitmap output = null;
        try {
            output = BitmapFactory.decodeFile(path);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return output;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Image)) return false;
        Image recording = (Image) o;
        return Objects.equals(getPath(), recording.getPath()) &&
                Objects.equals(getName(), recording.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPath(), getName());
    }

    @Override
    public String toString() {
        return "Image{" +
                "path='" + path + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    /**
     * Retorna un JSON representando la imagen.
     *
     * @return {@link String} - JSON de la Imagen.
     */
    public String toJSONString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("path", path);
            jsonObject.put("name", name);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return jsonObject.toString();
    }
}
