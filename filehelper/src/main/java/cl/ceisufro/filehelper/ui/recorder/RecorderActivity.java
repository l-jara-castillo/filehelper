package cl.ceisufro.filehelper.ui.recorder;

/**
 * Created by Luis Andrés Jara Castillo on 26-09-17.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.media.AudioFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import cl.ceisufro.filehelper.R;
import cl.ceisufro.filehelper.core.FileHelper;
import cl.ceisufro.filehelper.core.UriUtils;
import cl.ceisufro.filehelper.ui.utils.ConfirmDialog;
import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;

public class RecorderActivity extends AppCompatActivity
        implements View.OnClickListener, ConfirmDialog.OnOptionsClicked {

    private static final int REQUEST_RECORDING_PERMISSION = 1;
    private static final int REQUEST_CREATE_FILE = 19281;
    /**
     * Registry Object from activity
     */
    private int MAX_TIME = 60;
    private int actualTime = 0;
    private RecorderConfig recorderConfig;
    /**
     * Media omRecorder instance
     */
    private MediaRecorder mRecorder = null;
    private omrecorder.Recorder omRecorder = null;
    /**
     * This is the output file for our picture.
     */
    private ArrayList<Recording> recordings = new ArrayList<>();
    private Recording recording;
    /**
     * Elementos Visuales
     */
    private Chronometer chronometer;
    private FloatingActionButton recordingFab;
    private FloatingActionButton deleteFab;
    private FloatingActionButton saveFab;
    private FloatingActionButton playFab;
    private TextView maxTimeTextView;

    /**
     * MISC
     */
    private MediaPlayer mediaPlayer;
    private boolean isRecording;
    private Handler handler;
    private Runnable runnable;
    private File fileTree;
    private File recordingFile;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        if (savedInstanceState != null) {
            recorderConfig = (RecorderConfig) savedInstanceState.getSerializable("SAVED_RECORDERCONFIG");
            recordings = (ArrayList<Recording>) savedInstanceState.getSerializable("SAVED_RECORDINGS");
            recording = (Recording) savedInstanceState.getSerializable("SAVED_RECORDING");
        } else if (getIntent().hasExtra("RECORDERCONFIG")) {
            recorderConfig = (RecorderConfig) getIntent().getExtras().getSerializable("RECORDERCONFIG");
        } else {
            finish();
        }
        ImageButton closeImageButton = findViewById(R.id.close_imagebutton);
        closeImageButton.setOnClickListener(this);
        deleteFab = findViewById(R.id.delete_recording_fab);
        deleteFab.setOnClickListener(this);
        playFab = findViewById(R.id.play_recording_fab);
        playFab.setOnClickListener(this);
        saveFab = findViewById(R.id.save_recorging_fab);
        saveFab.setOnClickListener(this);
        chronometer = findViewById(R.id.recording_chronometer);
        maxTimeTextView = findViewById(R.id.max_time_textview);
        if (recorderConfig.getMaxDuration() > 0) {
            maxTimeTextView.setVisibility(View.VISIBLE);
        }
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isRecording) {
                    actualTime++;
                    if (MAX_TIME > 0 && actualTime >= MAX_TIME) {
                        stopRecording();
                    }
                    showTimeRemaining();
                    startTickCounter();
                }
            }
        };
        handler = new Handler();
        MAX_TIME = recorderConfig.getMaxDuration();
        showTimeRemaining();
        recordingFab = findViewById(R.id.start_recording_fab);
        recordingFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSuccess)));
        recordingFab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN && (MAX_TIME == 0 || actualTime < MAX_TIME)) {
                    Calendar date = Calendar.getInstance();
                    String fileName = "audio_" + date.getTimeInMillis();
                    if (recordingFile == null) {
                        recordingFile = FileHelper.createNewEmptyFile(fileTree.getPath(), fileName, "wav");
                    }
                    if (recordingFile != null) {
                        recording = new Recording(recordingFile.getPath(), fileName);
                        recordingFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDanger)));
                        startRecording();
                    } else {
                        showFileWarningDialog();
                    }
                    return true;
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    stopRecording();
                    return true;
                }
                return false;
            }
        });
        enableOptionsLayout(false);
        resolveDirectory();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (recorderConfig.isRequestingPermissions()) {
            if (shouldRequestPermissions()) {
                requestPermissions();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("SAVED_RECORDERCONFIG", recorderConfig);
        outState.putSerializable("SAVED_RECORDINGS", recordings);
        outState.putSerializable("SAVED_RECORDING", recording);
    }

    private boolean resolveDirectory() {
        String path;
        if (!TextUtils.isEmpty(recorderConfig.getDirectory())) {
            String dir = recorderConfig.getDirectory();
            dir = dir.startsWith("/") ? dir.replaceFirst("/", "") : dir;
            dir = FileHelper.getParentDirectoryFromFile(new File(dir));
            String mainStoragePath = FileHelper.getInternalMainStorageDirectory(this).getPath();
            if (!recorderConfig.isUsingInternalStorage() &&
                    FileHelper.isExternalStorageMounted() &&
                    !FileHelper.isExternalStorageReadOnly()) {
                mainStoragePath = FileHelper.getExternalMainStorageDirectory(this).getPath();
            }
            if (!recorderConfig.getDirectory().contains(mainStoragePath)) {
                path = mainStoragePath + File.separator + dir;
            } else {
                path = dir;
            }
        } else {
            path = FileHelper.getInternalStorageDirectory(this).getPath();
            if (!recorderConfig.isUsingInternalStorage() &&
                    FileHelper.isExternalStorageMounted() &&
                    !FileHelper.isExternalStorageReadOnly()) {
                path = FileHelper.getExternalStorageDirectory(this).getPath();
            }
        }
        fileTree = new File(path);
        if (!fileTree.exists()) {
            if (!fileTree.mkdirs()) {
                showFileWarningDialog();
                return false;
            }
        } else if (!fileTree.canWrite()) {
            showFileWarningDialog();
            return false;
        }
        return true;
    }

    private void showFileWarningDialog() {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.show(getFragmentManager(), "Confirmar");
    }

    @Override
    public void onPositiveClicked() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.setType("audio/wav");
        String title = "audio" + Calendar.getInstance().getTimeInMillis() + ".wav";
        intent.putExtra(Intent.EXTRA_TITLE, title);
        startActivityForResult(intent, REQUEST_CREATE_FILE);
    }

    @Override
    public void onNegativeClicked() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CREATE_FILE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                String path = UriUtils.getPath(this, uri);
                recordingFile = new File(path);
            }
        }
    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissionsList = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.RECORD_AUDIO);
            }
            if (permissionsList.size() > 0) {
                String permissions[] = permissionsList.toArray(new String[0]);
                requestPermissions(permissions, REQUEST_RECORDING_PERMISSION);
            }
        }
    }

    private boolean shouldRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissionsList = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.RECORD_AUDIO);
            }
            return permissionsList.size() > 0;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_RECORDING_PERMISSION) {
            if (grantResults.length == 0) {
                Toast.makeText(this, getString(R.string.permisos_grabacion_necesarios), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                boolean granted = true;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        granted = false;
                    }
                }
                if (!granted) {
                    Toast.makeText(this, getString(R.string.permisos_grabacion_necesarios), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            closeRecorder();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showTimeRemaining() {
        int timeLeft = actualTime > MAX_TIME ? MAX_TIME : MAX_TIME - actualTime;
        int minutes = (timeLeft / 60);
        int seconds = timeLeft >= 60 ? timeLeft % 60 : timeLeft;
        String time = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
        maxTimeTextView.setText(" / " + time);
    }

    private void closeRecorder() {
        Intent data = new Intent();
        data.putExtra("data", recordings);
        setResult(RESULT_OK, data);
        finish();
    }

    private void saveRecording() {
        stopPlayback();
        recordingFab.setEnabled(true);
        recordingFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSuccess)));
        enableOptionsLayout(false);
        recordings.add(recording);
        if (recorderConfig.isSingle()) {
            closeRecorder();
        }
        Toast.makeText(this, "Audio Guardado!", Toast.LENGTH_SHORT).show();
        recordingFile = null;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.close_imagebutton) {
            closeRecorder();
        } else if (id == R.id.delete_recording_fab) {
            File file = new File(recording.getPath());
            int auxtime = FileHelper.getAudioDurationFromFile(this, file);
            actualTime -= auxtime;
            actualTime = actualTime > MAX_TIME ? MAX_TIME : actualTime;
            showTimeRemaining();
            if (file.delete()) {
                Toast.makeText(this, "Audio Descartado!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "No se ha podido eliminar el audio!", Toast.LENGTH_SHORT).show();
            }
            recordingFab.setEnabled(true);
            recordingFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSuccess)));
            enableOptionsLayout(false);
            recordingFile = null;
        } else if (id == R.id.play_recording_fab) {
            try {
                if (mediaPlayer == null || !mediaPlayer.isPlaying()) {
                    startPlayback();
                } else {
                    stopPlayback();
                    stopChronometer();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.save_recorging_fab) {
            saveRecording();
        }
    }

    private void startRecording() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            stopPlayback();
            stopChronometer();
        }
        AudioRecordConfig audioRecordConfig = new AudioRecordConfig.Default(MediaRecorder.AudioSource.MIC,
                AudioFormat.ENCODING_PCM_16BIT, AudioFormat.CHANNEL_IN_MONO, 44100);
        PullTransport pullTransport = new PullTransport.Default(new PullableSource.Default(audioRecordConfig));
        File file = new File(recording.getPath());
        omRecorder = OmRecorder.wav(pullTransport, file);
        omRecorder.startRecording();
        isRecording = true;
        startChronometer();
        startTickCounter();
    }

    private void stopRecording() {
        try {
            omRecorder.stopRecording();
            isRecording = false;
            stopChronometer();
            stopTickCounter();
            recordingFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDisabled)));
            recordingFab.setEnabled(false);
            enableOptionsLayout(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startChronometer() {
        try {
            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopChronometer() {
        try {
            chronometer.stop();
            chronometer.setBase(SystemClock.elapsedRealtime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startTickCounter() {
        handler.postDelayed(runnable, 1000);
    }

    private void stopTickCounter() {
        handler.removeCallbacks(runnable);
    }


    private void startPlayback() {
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopChronometer();
                    playFab.setImageResource(R.drawable.ic_play_arrow);
                }
            });
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                    startChronometer();
                    playFab.setImageResource(R.drawable.ic_stop);
                }
            });
            mediaPlayer.setDataSource(recording.getPath());
            mediaPlayer.prepare();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopPlayback() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void enableOptionsLayout(boolean enable) {
        if (enable) {
            deleteFab.setEnabled(true);
            saveFab.setEnabled(true);
            playFab.setEnabled(true);
            deleteFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDanger)));
            saveFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryB)));
            playFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorSuccess)));
        } else {
            deleteFab.setEnabled(false);
            saveFab.setEnabled(false);
            playFab.setEnabled(false);
            deleteFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDisabled)));
            saveFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDisabled)));
            playFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDisabled)));

        }
    }

}
