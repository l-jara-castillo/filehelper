package cl.ceisufro.filehelper.ui.recorder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Luis Andrés Jara Castillo on 10-10-17.
 */

public class Recording implements Serializable {
    private String path;
    private String name;

    public Recording(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recording)) return false;
        Recording recording = (Recording) o;
        return Objects.equals(getPath(), recording.getPath()) &&
                Objects.equals(getName(), recording.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPath(), getName());
    }

    @Override
    public String toString() {
        return "Recording{" +
                "path='" + path + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public String toJSONString() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("path", path);
        jsonObject.put("name", name);
        return jsonObject.toString();
    }
}
