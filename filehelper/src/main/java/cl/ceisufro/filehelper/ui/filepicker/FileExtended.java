package cl.ceisufro.filehelper.ui.filepicker;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.Objects;

/**
 * Creado por Luis Andrés Jara Castillo on 13-10-17.
 * <p>
 * Clase utilitaria para el Selector de Archivos. Su principal uso
 * es la de permitir determinar si el archivo ha sido seleccionado o
 * si este tiene un directorio previo al cual se pueda visualizar.
 * Esta clase es un hijo de la clase {@link File} y por ende
 * permite toda la funcionalidad de dicha clase y que facilita el retornar
 * los resultados a la actividad una vez finalizada la selección de archivos.
 */

class FileExtended extends File {
    private boolean selected;
    private boolean previousDir;

    /**
     * Constructor de la clase.
     *
     * @param path        {@link String} - Ruta del archivo o directorio.
     * @param selected    boolean - Indicador si este fichero ha sido seleccionado para ser retornado.
     * @param previousDir boolean - Indicador si este fichero es un directorio previo de algun archivo.
     */
    FileExtended(String path, boolean selected, boolean previousDir) {
        super(path);
        this.selected = selected;
        this.previousDir = previousDir;
    }

    /**
     * Constructor de la clase.
     *
     * @param file        {@link File} - Archivo o directorio para inicializar a la super clase.
     * @param selected    boolean - Indicador si este fichero ha sido seleccionado para ser retornado.
     * @param previousDir boolean - Indicador si este fichero es un directorio previo de algun archivo.
     */
    FileExtended(File file, boolean selected, boolean previousDir) {
        super(file.getPath());
        this.selected = selected;
        this.previousDir = previousDir;
    }

    /**
     * Constructor de la clase.
     *
     * @param path        {@link String} - Ruta del archivo o directorio.
     * @param previousDir boolean - Indicador si este fichero es un directorio previo de algun archivo.
     */
    FileExtended(String path, boolean previousDir) {
        super(path);
        this.selected = false;
        this.previousDir = previousDir;
    }

    /**
     * Constructor de la clase.
     *
     * @param pathname {@link String} - Ruta del archivo o directorio.
     */
    FileExtended(@NonNull String pathname) {
        super(pathname);
        this.selected = false;
        this.previousDir = false;
    }

    /**
     * Constructor de la clase.
     *
     * @param file {@link File} - Archivo o directorio para inicializar a la super clase.
     */
    FileExtended(File file) {
        super(file.getPath());
        this.previousDir = false;
    }

    /**
     * Retorna el estado de selección de archivo.
     *
     * @return boolean - true si el fichero ha sido seleccionado; false si no se ha seleccionado
     * el archivo.
     */
    boolean isSelected() {
        return selected;
    }

    /**
     * Establece si el archivo ha sido seleccionado.
     *
     * @param selected boolean - Estado de selección de archivo.
     */
    void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Retorna si el archivo es un directorio previo de un archivo.
     *
     * @return boolean - true si el directorio corresponde a un directorio previo de un archivo;
     * false si el archivo es común.
     */
    public boolean isPreviousDir() {
        return previousDir;
    }

    /**
     * Establece si el archivo es un directorio previo de algún otro archivo.
     *
     * @param previousDir boolean - Valor para el atributo,
     */
    public void setPreviousDir(boolean previousDir) {
        this.previousDir = previousDir;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileExtended)) return false;
        if (!super.equals(o)) return false;
        FileExtended that = (FileExtended) o;
        return isSelected() == that.isSelected() &&
                isPreviousDir() == that.isPreviousDir();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isSelected(), isPreviousDir());
    }

    @Override
    public String toString() {
        return "PickedFile{" +
                "selected=" + selected +
                ", previousDir=" + previousDir +
                "} " + super.toString();
    }
}
