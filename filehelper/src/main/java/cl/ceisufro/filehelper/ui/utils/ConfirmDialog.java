package cl.ceisufro.filehelper.ui.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by luis on 09-11-17.
 */

public class ConfirmDialog extends DialogFragment {
    private OnOptionsClicked onOptionsClicked;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onOptionsClicked = (OnOptionsClicked) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement  OnOptionsListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("No es posible escribir en la carpeta, debido a que requiere que el fichero se cree manualmente por el usuario. ¿Desea crear el archivo?");
        builder.setPositiveButton("Crear", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onOptionsClicked.onPositiveClicked();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onOptionsClicked.onNegativeClicked();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }

    public interface OnOptionsClicked {
        void onPositiveClicked();

        void onNegativeClicked();
    }
}
