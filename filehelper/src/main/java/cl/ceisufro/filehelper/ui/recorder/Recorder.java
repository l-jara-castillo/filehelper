package cl.ceisufro.filehelper.ui.recorder;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

/**
 * Created by Luis Andrés Jara Castillo on 10-10-17.
 */

public class Recorder {
    private Activity activity;
    private Fragment fragment;
    private int maxDuration;
    private String path;
    private boolean single;
    private boolean internalStorage;
    private boolean requestPermissions;

    private Recorder(Builder builder) {
        this.activity = builder.activity;
        this.fragment = builder.fragment;
        this.maxDuration = builder.maxDuration;
        this.path = builder.path;
        this.single = builder.single;
        this.internalStorage = builder.internalStorage;
        this.requestPermissions = builder.requestPermissions;
    }

    public Activity getActivity() {
        if (activity != null) {
            return activity;
        } else {
            return fragment.getActivity();
        }
    }

    public Fragment getFragment() {
        return fragment;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public String getPath() {
        return path;
    }

    public boolean isSingle() {
        return single;
    }

    public boolean isUsingInternalStorage() {
        return internalStorage;
    }

    public boolean isRequestingPermissions() {
        return requestPermissions;
    }

    public static Builder newBuilder(Activity activity) {
        return new Builder(activity);
    }

    public static Builder newBuilder(Fragment fragment) {
        return new Builder(fragment);
    }

    public void start(int requestCode) {
        if (activity != null) {
            activity.startActivityForResult(getIntent(), requestCode);
        } else {
            fragment.startActivityForResult(getIntent(), requestCode);
        }
    }

    private Intent getIntent() {
        Intent intent;
        if (activity != null) {
            intent = new Intent(activity, RecorderActivity.class);
        } else {
            intent = new Intent(fragment.getActivity(), RecorderActivity.class);
        }
        intent.putExtra("RECORDERCONFIG", new RecorderConfig(this));
        return intent;
    }

    public static ArrayList<Recording> getRecordings(Intent intent) {
        ArrayList<Recording> recordings = new ArrayList<>();
        if (intent != null && intent.hasExtra("data")) {
            recordings = (ArrayList<Recording>) intent.getExtras().getSerializable("data");
        }
        return recordings;
    }

    public static class Builder {
        private Activity activity;
        private Fragment fragment;
        private int maxDuration = 0;
        private String path;
        private boolean single = true;
        private boolean internalStorage = true;
        private boolean requestPermissions = false;

        public Builder(Activity activity) {
            this.activity = activity;
        }

        public Builder(Fragment fragment) {
            this.fragment = fragment;
        }

        public Builder setMaxDuration(int maxDuration) {
            this.maxDuration = maxDuration;
            return this;
        }

        public Builder setSingleRecordingMode() {
            this.single = true;
            return this;
        }

        public Builder setMultiRecordingMode() {
            this.single = false;
            return this;
        }

        public Builder setInternalStorage() {
            this.internalStorage = true;
            return this;
        }

        public Builder setExternalStorage() {
            this.internalStorage = false;
            return this;
        }

        public Builder requestPermissions() {
            this.requestPermissions = true;
            return this;
        }

        public Builder setPath(String path) {
            this.path = path;
            return this;
        }

        public Recorder build() {
            return new Recorder(this);
        }

        public Recorder start(int requestCode) {
            Recorder recorder = new Recorder(this);
            recorder.start(requestCode);
            return recorder;
        }
    }
}
