package cl.ceisufro.filehelper.ui.recorder;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Luis Andrés Jara Castillo on 10-10-17.
 */

class RecorderConfig implements Serializable {
    private int maxDuration;
    private String directory;
    private boolean single;
    private boolean internalStorage;
    private boolean requestPermissions;

    RecorderConfig(Recorder recorder) {
        this.maxDuration = recorder.getMaxDuration();
        this.directory = recorder.getPath();
        this.single = recorder.isSingle();
        this.internalStorage = recorder.isUsingInternalStorage();
        this.requestPermissions = recorder.isRequestingPermissions();
    }

    int getMaxDuration() {
        return maxDuration;
    }

    String getDirectory() {
        return directory;
    }

    boolean isSingle() {
        return single;
    }

    boolean isUsingInternalStorage() {
        return internalStorage;
    }

    boolean isRequestingPermissions() {
        return requestPermissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecorderConfig)) return false;
        RecorderConfig that = (RecorderConfig) o;
        return getMaxDuration() == that.getMaxDuration() &&
                isSingle() == that.isSingle() &&
                isUsingInternalStorage() == that.isUsingInternalStorage() &&
                isRequestingPermissions() == that.isRequestingPermissions() &&
                Objects.equals(getDirectory(), that.getDirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMaxDuration(), getDirectory(), isSingle(), isRequestingPermissions());
    }

    @Override
    public String toString() {
        return "RecorderConfig{" +
                "maxDuration=" + maxDuration +
                ", directory='" + directory + '\'' +
                ", isSingleRecordingMode=" + single +
                ", isUsingInternalStorage=" + internalStorage +
                ", requestPermissions=" + requestPermissions +
                '}';
    }
}
