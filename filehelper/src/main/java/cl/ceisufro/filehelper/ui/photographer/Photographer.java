package cl.ceisufro.filehelper.ui.photographer;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

/**
 * Creado por Luis Andrés Jara Castillo on 26-09-17.
 * <p>
 * {@link Photographer} es la clase constructora que servirá de interfaz
 * para el usuario de la biblioteca. Esta clase tiene como función
 * establecer los parámetros de funcionamiento y lanzar el widget.
 * Para lanzar el widget se debe llamar el método {@link Photographer#start(int)}, Recordando
 * que el parámetro de dicho método es un "request code" el cual se usara para entregar la respuesta.
 * La respuesta que se obtiene de este Selector es una {@link ArrayList}<{@link Image}> que
 * corresponden a las imágenes capturadas. Esta Respuesta
 * es devuelta a través de un
 * {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}
 * , para obtener la lista de imágenes se debe llamar al método estático
 * {@link Photographer#getImages(Intent)} el cual retorna un {@link ArrayList}<{@link Image}>.
 */

public class Photographer {
    private Activity activity;
    private Fragment fragment;
    private boolean single;
    private boolean internalStorage;
    private boolean requestPermissions;
    private int maxPhotos;
    private String directory;
    private String fileName;

    /**
     * Constructor de la clase. Esto existe para que el Builder pueda construir la instancia de la
     * clase y no desde fuera.
     *
     * @param builder {@link Builder} - Builder para crear la instancia de la clase.
     */
    private Photographer(Builder builder) {
        this.activity = builder.activity;
        this.fragment = builder.fragment;
        this.single = builder.single;
        this.internalStorage = builder.internalStorage;
        this.requestPermissions = builder.requestPermissions;
        this.maxPhotos = builder.maxPhotos;
        this.directory = builder.directory;
        this.fileName = builder.fileName;
    }

    /**
     * Retorna la actividad sobre la cual se lanzó el widget.
     *
     * @return {@link Activity} - Activity sobre la cual esta posada el widget.
     */
    public Activity getActivity() {
        if (activity != null) {
            return activity;
        } else {
            return fragment.getActivity();
        }
    }

    /**
     * Retorna el fragmento sobre la cual se lanzo el Selector de archivos.
     *
     * @return {@link Fragment} - Fragment sobre el esta posada el Selector de archivos o null
     * si no se utilizo un Fragment.
     */
    public Fragment getFragment() {
        return fragment;
    }

    /**
     * Retorna si esta en modo de una sola captura o de multiples capturas.
     *
     * @return boolean - true si esta en modo de una captura o false si es modo de multiples capturas.
     */
    public boolean isSingle() {
        return single;
    }

    public boolean isUsingInternalStorage() {
        return internalStorage;
    }

    /**
     * Retornar si se ha establecido que la biblioteca pida permisos de lectura de archivos
     * cuando sea lanzada.
     *
     * @return true si se ha establecido esta funcionalidad o false si no se ha establecido. Si
     * no se ha establecido se debe considerar que la aplicación los pida antes de lanzar el
     * Selector para evitar problemas.
     */
    public boolean isRequestingPermissions() {
        return requestPermissions;
    }

    /**
     * Devuelve la cantidad de imágenes que se pueden capturar. La cámara
     * automáticamente prohibirá capturar más imágenes que
     * lo este valor indique.
     *
     * @return int - Cantidad de imágenes que esta permitido capturar.
     */
    public int getMaxPhotos() {
        return maxPhotos;
    }

    /**
     * Retorna la ruta en la cual se almacenaran las imágenes.
     *
     * @return {@link String} - Ruta donde se pretende guardar las imágenes capturadas.
     */
    public String getDirectory() {
        return directory;
    }

    /**
     * Retorna el nombre de la imagen que recibirá en el modo de una imagen.
     *
     * @return {@link String} - Nombre para el archivo a guardar.
     */
    public String getFileName() {
        return fileName;
    }

    public static Builder newBuilder(Activity activity) {
        return new Builder(activity);
    }

    public static Builder newBuilder(Fragment fragment) {
        return new Builder(fragment);
    }

    /**
     * Lanza el widget usando el parámetro para retornar la lista de imágenes
     * a través de {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}.
     *
     * @param requestCode int - Código para el cual se retornara la lista de imágenes.
     */
    public void start(int requestCode) {
        if (activity != null) {
            activity.startActivityForResult(getIntent(), requestCode);
        } else {
            fragment.startActivityForResult(getIntent(), requestCode);
        }
    }

    /**
     * Crea el intent que se usara para lanzar el widget.
     *
     * @return {@link Intent} - Intent cargado con los parámetros de esta clase.
     */
    private Intent getIntent() {
        Intent intent;
        if (activity != null) {
            intent = new Intent(activity, PhotographerActivity.class);
        } else {
            intent = new Intent(fragment.getActivity(), PhotographerActivity.class);
        }
        intent.putExtra("PHOTOGRAPHERCONFIG", new PhotographerConfig(this));
        return intent;
    }

    /**
     * Método estático que extrae y retorna la lista de imágenes contenida en un {@link Intent}.
     *
     * @param intent {@link Intent} - Intent obtenido del método {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}
     *               que contiene la respuesta del Selector.
     * @return {@link ArrayList}<{@link Image}> - Lista de imágenes extraídas.
     */
    public static ArrayList<Image> getImages(Intent intent) {
        ArrayList<Image> images = new ArrayList<>();
        if (intent != null && intent.hasExtra("data")) {
            images = (ArrayList<Image>) intent.getExtras().getSerializable("data");
        }
        return images;
    }

    /**
     * Builder del widget.
     * <br>
     * Permite crear la instancia del Widget con las configuraciones
     * deseadas para la necesidad del caso.
     */
    public static class Builder {
        private Activity activity;
        private Fragment fragment;
        private boolean single = true;
        private boolean requestPermissions = false;
        private boolean internalStorage = true;
        private int maxPhotos = 0;
        private String directory;
        private String fileName;

        /**
         * Constructor de la clase Builder.
         *
         * @param activity {@link Activity} - Activity sobre la cual se añadirá el widget.
         */
        public Builder(Activity activity) {
            this.activity = activity;
        }

        /**
         * Constructor de la clase Builder.
         *
         * @param fragment {@link Fragment} - Fragment del cual se obtiene la Activity para añadir
         *                 el widget.
         */
        public Builder(Fragment fragment) {
            this.fragment = fragment;
        }


        /**
         * Establece si se capturara solo una imagen. Solo se permitirá capturar un archivo
         * incluso aún si se ha establecido una cantidad de archivos máxima.
         * <br>
         * Este es el modo por defecto.
         */
        public Builder setSingleImageMode() {
            this.single = true;
            return this;
        }

        /**
         * Establece si se capturara mas de una imagen.
         * <br>
         * Por defecto el modo de  es el de una imagen.
         */
        public Builder setMultiImageMode() {
            this.single = false;
            return this;
        }

        /**
         * Establece si se creara los archivos en el directorio interno de la memoria
         * del dispositivo.
         */
        public Builder setInternalStorage() {
            this.internalStorage = true;
            return this;
        }

        /**
         * Establece si se creara los archivos en la memoria extraíble del dispositivo.
         * Si no se ha insertado una memoria extraíble, los archivos serán creados en la memoria
         * interna por defecto.
         */
        public Builder setExternalStorage() {
            this.internalStorage = false;
            return this;
        }

        /**
         * Establece si el widget se encargara de pedir permisos pertinentes (Escritura de archivos, Uso de Cámara)
         * si no han sido asignados previamente a la aplicación.
         */
        public Builder requestPermissions() {
            this.requestPermissions = true;
            return this;
        }

        /**
         * Establece un maximo de imágenes para capturar. Si no se establece
         * este valor no existirá un limite de imágenes para seleccionar.
         * <br>
         * Por defecto no existe limite de imágenes.
         *
         * @param maxPhotos int - Cantidad de imágenes. Dejar sin valor para no establecer un limite.
         */
        public Builder setMaxPhotos(int maxPhotos) {
            this.maxPhotos = maxPhotos;
            return this;
        }

        /**
         * Establece la ruta en la cual se almacenaran las imágenes.
         *
         * @param path {@link String} - Ruta del directorio en la cual se almacenaran las imágenes.
         *             Si se ingresa una ruta de un archivo, las imágenes se
         *             guardaran junto a el.
         */
        public Builder setPath(String path) {
            this.directory = path;
            return this;
        }

        /**
         * Establece el nombre para la imagen en el modo de una imagen.
         *
         * @param fileName {@link String} - Nombre de la imagen sin extension. Evite usar espacios
         *                 en el nombre.
         */
        public Builder setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        /**
         * Construye la instancia de {@link Photographer} con la configuración establecida.
         *
         * @return {@link Photographer} - Instancia construida a partir de la configuración.
         */
        public Photographer build() {
            return new Photographer(this);
        }

        /**
         * Construye y lanza el {@link Photographer} usando un Request Code para retornar las imágenes
         * a través de un {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}.
         *
         * @param requestCode int - Código de solicitud para retornar los las imágenes capturadas.
         * @return {@link Photographer} - Instancia construida a partir de la configuración.
         */
        public Photographer start(int requestCode) {
            Photographer photographer = new Photographer(this);
            photographer.start(requestCode);
            return photographer;
        }
    }
}
