package cl.ceisufro.filehelper.ui.filepicker;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Creado por Luis Andrés Jara Castillo on 26-09-17.
 * <p>
 * FilePicker es la clase constructora que servirá de interfaz
 * para el usuario de la biblioteca. Esta clase tiene como función
 * establecer los parámetros de funcionamiento y lanzar el Selector
 * de archivos.
 * Para lanzar el Selector se debe llamar el método {@link FilePicker#start(int)}, Recordando
 * que el parámetro de dicho método es un "request code" el cual se usara para entregar la respuesta.
 * La respuesta que se obtiene de este Selector es una {@link ArrayList}<{@link File}> que
 * corresponden a los ficheros seleccionados. Esta Respuesta
 * es devuelta a través de un
 * {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}
 * , para obtener la lista de archivos se debe llamar al método estático
 * {@link FilePicker#getFilesFromIntent(Intent)} el cual retorna un {@link ArrayList}<{@link File}.
 */

public class FilePicker {
    private Activity activity;
    private Fragment fragment;
    private int maxFiles;
    private boolean single;
    private boolean requestPermissions;
    private String path;
    private String actionBarTitle;
    private ArrayList<String> mimeTypes;
    private ArrayList<String> mimeSubTypes;

    /**
     * Constructor privado de la clase.
     * Inicializa los valores usando los valores de la instancia de {@link Builder}
     * entregada como parámetro.
     *
     * @param builder {@link Builder} - Instancia de Builder para obtener los valores.
     */
    private FilePicker(Builder builder) {
        this.activity = builder.activity;
        this.fragment = builder.fragment;
        this.single = builder.single;
        this.requestPermissions = builder.requestPermissions;
        this.maxFiles = builder.maxFiles;
        this.path = builder.path;
        this.actionBarTitle = builder.actionBarTitle;
        this.mimeTypes = builder.mimeTypes;
        this.mimeSubTypes = builder.mimeSubTypes;
    }

    /**
     * Retorna la actividad sobre la cual se lanzó el Selector de archivos.
     *
     * @return {@link Activity} - Activity sobre la cual esta posada el Selector de archivos.
     */
    public Activity getActivity() {
        if (activity != null) {
            return activity;
        } else {
            return fragment.getActivity();
        }
    }


    /**
     * Retorna el fragmento sobre la cual se lanzo el Selector de archivos.
     *
     * @return {@link Fragment} - Fragment sobre el esta posada el Selector de archivos o null
     * si no se utilizo un Fragment.
     */
    public Fragment getFragment() {
        return fragment;
    }

    /**
     * Devuelve la cantidad de archivos que se pueden seleccionar. El
     * Selector automáticamente prohibirá seleccionar mas archivos que
     * lo este valor indique.
     *
     * @return int - Cantidad de archivo que esta permitido seleccionar.
     */
    public int getMaxFiles() {
        return maxFiles;
    }

    /**
     * Retorna si esta en modo de un archivo o de multiples archivos.
     *
     * @return boolean - true si esta en modo de un archivo o false si es modo de multiples archivos.
     */
    public boolean isSingle() {
        return single;
    }

    /**
     * Retornar si se ha establecido que la biblioteca pida permisos de lectura de archivos
     * cuando sea lanzada.
     *
     * @return true si se ha establecido esta funcionalidad o false si no se ha establecido. Si
     * no se ha establecido se debe considerar que la aplicación los pida antes de lanzar el
     * Selector para evitar problemas.
     */
    public boolean isRequestingPermission() {
        return requestPermissions;
    }

    /**
     * Retorna la ruta desde la cual se lanzara el Selector de archivos.
     *
     * @return {@link String} - Ruta inicial para el Selector.
     */
    public String getPath() {
        return path;
    }

    /**
     * Retorna los tipos de archivos permitidos para seleccionar.
     *
     * @return {@link ArrayList}<{@link String}> - Lista de tipos de archivos que se mostraran.
     */
    public ArrayList<String> getMimeTypes() {
        return mimeTypes;
    }

    /**
     * Retorna los subtipos de archivos permitidos para seleccionar.
     *
     * @return {@link ArrayList}<{@link String}> - Lista de subtipos de archivos que se mostraran.
     */
    public ArrayList<String> getMimeSubTypes() {
        return mimeSubTypes;
    }

    /**
     * Retorna el titulo que se mostrara en la AppBar de la aplicación.
     *
     * @return {@link String} - Titulo de la barra.
     */
    public String getActionBarTitle() {
        return actionBarTitle;
    }

    public static Builder newBuilder(Activity activity) {
        return new Builder(activity);
    }

    public static Builder newBuilder(Fragment fragment) {
        return new Builder(fragment);
    }

    /**
     * Lanza el Selector de archivos usando el parámetro para retornar la lista de archivos
     * a través de {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}.
     *
     * @param requestCode int - Código para el cual se retornara la lista de archivos.|
     */
    public void start(int requestCode) {
        if (activity != null) {
            activity.startActivityForResult(getIntent(), requestCode);
        } else {
            fragment.startActivityForResult(getIntent(), requestCode);
        }
    }

    /**
     * Crea el intent que se usara para lanzar el Selector de archivos.
     *
     * @return {@link Intent} - Intent cargado con los parámetros de esta clase.
     */
    private Intent getIntent() {
        Intent intent;
        if (activity != null) {
            intent = new Intent(activity, FilePickerActivity.class);
        } else {
            intent = new Intent(fragment.getActivity(), FilePickerActivity.class);
        }
        intent.putExtra("FILEPICKERCONFIG", new FilePickerConfig(this));
        return intent;
    }

    /**
     * Método estático que extrae y retorna la lista de archivos contenida en un {@link Intent}.
     *
     * @param intent {@link Intent} - Intent obtenido del método {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}
     *               que contiene la respuesta del Selector.
     * @return {@link ArrayList}<{@link File}> - Lista de archivos extraídos.
     */
    public static ArrayList<File> getFilesFromIntent(Intent intent) {
        ArrayList<File> recordings = new ArrayList<>();
        if (intent != null && intent.hasExtra("data")) {
            recordings = (ArrayList<File>) intent.getExtras().getSerializable("data");
        }
        return recordings;
    }

    /**
     * Builder del Selector de archivos.
     * <br>
     * Permite crear la instancia del Widget con las configuraciones
     * deseadas para la necesidad del caso.
     */
    public static class Builder {
        private Activity activity;
        private Fragment fragment;
        private int maxFiles = 0;
        private boolean single = true;
        private boolean requestPermissions = false;
        private String path;
        private String actionBarTitle;
        private ArrayList<String> mimeTypes;
        private ArrayList<String> mimeSubTypes;

        /**
         * Constructor de la clase Builder.
         *
         * @param activity {@link Activity} - Activity sobre la cual se añadirá el widget.
         */
        public Builder(Activity activity) {
            this.activity = activity;
            this.mimeTypes = new ArrayList<>();
            this.mimeSubTypes = new ArrayList<>();
        }

        /**
         * Constructor de la clase Builder.
         *
         * @param fragment {@link Fragment} - Fragment del cual se obtiene la Activity para añadir
         *                 el widget.
         */
        public Builder(Fragment fragment) {
            this.fragment = fragment;
            this.mimeTypes = new ArrayList<>();
            this.mimeSubTypes = new ArrayList<>();
        }

        /**
         * Establece un maximo de archivos para seleccionar. Si no se establece
         * este valor no existirá un limite de archivos para seleccionar.
         * <br>
         * Por defecto no existe limite de archivos.
         *
         * @param maxFiles int - Cantidad de archivos. Dejar sin valor para no establecer un limite.
         */
        public Builder setMaxFiles(int maxFiles) {
            this.maxFiles = maxFiles;
            return this;
        }

        /**
         * Establece si se seleccionara solo un archivo. Solo se permitirá seleccionar un archivo
         * incluso aún si se ha establecido una cantidad de archivos máxima.
         * <br>
         * Este es el modo por defecto.
         */
        public Builder setSingleFileMode() {
            this.single = true;
            return this;
        }

        /**
         * Establece si se seleccionara mas de un archivo.
         * <br>
         * Por defecto el modo de un archivo es el de un archivo.
         */
        public Builder setMultiFileMode() {
            this.single = false;
            return this;
        }

        /**
         * Establece si el widget se encargara de pedir permisos pertinentes (Lectura de archivos)
         * si no han sido asignados previamente a la aplicación.
         */
        public Builder requestPermissions() {
            this.requestPermissions = true;
            return this;
        }

        /**
         * Establece un titulo personalizado para el AppBar de la aplicación.
         *
         * @param title {@link String} - Texto para el titulo del AppBar.
         */
        public Builder setActionBarTitle(String title) {
            this.actionBarTitle = title;
            return this;
        }

        /**
         * Establece la ruta en la cual se iniciará el Selector de Archivos.
         *
         * @param path {@link String} - Ruta del directorio en la cual iniciar. Si se ingresa
         *             una ruta de un archivo, se iniciara en la carpeta en la cual
         *             esta localizado.
         */
        public Builder setPath(String path) {
            this.path = path;
            return this;
        }

        /**
         * Añade un filtro a los archivos que serán visibles para seleccionar.
         * Usando el formato de MimeType ("tipo/subtipo") se debe usar como parámetro el "tipo"
         * al cual deben pertenecer los archivos.
         *
         * @param mimeType {@link String} - Tipo de archivos al cual deben pertenecer
         *                 los archivos para ser visibles.
         */
        public Builder addMimeType(String mimeType) {
            this.mimeTypes.add(mimeType);
            return this;
        }

        /**
         * Añade un filtro a los archivos que serán visibles para seleccionar.
         * Usando el formato de MimeType ("tipo/subtipo") se debe usar como parámetro el "tipo"
         * al cual deben pertenecer los archivos.
         * Ej: Si se ingresa "image, video" se permitirán seleccionar imágenes y videos.
         *
         * @param types {@link String} - Tipos archivos al cual deben pertenecer
         *              los archivos para ser visibles.
         */
        public Builder addMimeType(String... types) {
            if (types != null && types.length > 0) {
                this.mimeTypes.addAll(Arrays.asList(types));
            }
            return this;
        }

        /**
         * Añade un filtro a los archivos que serán visibles para seleccionar.
         * Usando el formato de MimeType ("tipo/subtipo") se debe usar como parámetro el "subtipo"
         * al cual deben pertenecer los archivos.
         * Ej: "jpeg" solo se podrán ver imágenes y que sean de subtipo "jpeg".
         *
         * @param mimeSubType {@link String} - Subtipo de archivos al cual deben pertenecer
         *                    los archivos para ser visibles.
         */
        public Builder addMimeSubType(String mimeSubType) {
            this.mimeSubTypes.add(mimeSubType);
            return this;
        }

        /**
         * Añade un filtro a los archivos que serán visibles para seleccionar.
         * Usando el formato de MimeType ("tipo/subtipo") se debe usar como parámetro el "subtipo"
         * al cual deben pertenecer los archivos.
         * Ej: Si se ingresa "mp4, jpeg" como parámetro se permitirán seleccionar imágenes y videos que
         * correspondan a estos subtipos.
         *
         * @param subTypes {@link String} - Subtipos archivos al cual deben pertenecer
         *                 los archivos para ser visibles.
         */
        public Builder addMimeSubType(String... subTypes) {
            if (subTypes != null && subTypes.length > 0) {
                this.mimeSubTypes.addAll(Arrays.asList(subTypes));
            }
            return this;
        }

        /**
         * Construye la instancia de {@link FilePicker} con la configuración establecida.
         *
         * @return {@link FilePicker} - Instancia construida a partir de la configuración.
         */
        public FilePicker build() {
            return new FilePicker(this);
        }

        /**
         * Construye y lanza el {@link FilePicker} usando un Request Code para retornar los archivos
         * a través de un {@link android.support.v4.app.FragmentActivity#onActivityResult(int, int, Intent)}.
         *
         * @param requestCode int - Código de solicitud para retornar los archivos seleccionados.
         * @return {@link FilePicker} - Instancia construida a partir de la configuración.
         */
        public FilePicker start(int requestCode) {
            FilePicker filePicker = new FilePicker(this);
            filePicker.start(requestCode);
            return filePicker;
        }
    }
}
