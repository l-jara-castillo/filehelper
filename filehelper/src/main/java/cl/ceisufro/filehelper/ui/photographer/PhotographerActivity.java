package cl.ceisufro.filehelper.ui.photographer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import cl.ceisufro.filehelper.R;
import cl.ceisufro.filehelper.core.FileHelper;
import cl.ceisufro.filehelper.core.UriUtils;
import cl.ceisufro.filehelper.ui.utils.ConfirmDialog;

/**
 * Created by Luis Andrés Jara Castillo on 11-10-17.
 */

public class PhotographerActivity extends Activity implements ConfirmDialog.OnOptionsClicked {
    private static final int REQUEST_CAMERA_PERMISSIONS = 502;
    private static final int REQUEST_CAMERA_LAUNCH = 1002003;
    private static final int REQUEST_CREATE_FILE = 119129;
    private PhotographerConfig photographerConfig;
    private ArrayList<Image> images = new ArrayList<>();
    private Image image;
    private File fileTree;
    private File imageFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            photographerConfig = (PhotographerConfig) savedInstanceState.getSerializable("SAVED_PHOTOGRAPHERCONFIG");
            images = (ArrayList<Image>) savedInstanceState.getSerializable("SAVED_IMAGES");
            image = (Image) savedInstanceState.getSerializable("SAVED_IMAGE");
        } else if (getIntent().hasExtra("PHOTOGRAPHERCONFIG")) {
            photographerConfig = (PhotographerConfig) getIntent().getExtras().getSerializable("PHOTOGRAPHERCONFIG");
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (photographerConfig.isRequestingPermissions()) {
            if (shouldRequestPermissions()) {
                requestPermissions();
            } else if (photographerConfig.getMaxPhotos() == 0 ||
                    images.size() < photographerConfig.getMaxPhotos()) {
                if (resolveDirectory()) {
                    launchCamera();
                }
            } else {
                closePhotoghrapher(RESULT_OK);
            }
        } else {
            launchCamera();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("SAVED_PHOTOGRAPHERCONFIG", photographerConfig);
        outState.putSerializable("SAVED_IMAGES", images);
        outState.putSerializable("SAVED_IMAGE", image);
    }

    private boolean resolveDirectory() {
        String path;
        if (!TextUtils.isEmpty(photographerConfig.getDirectory())) {
            String dir = photographerConfig.getDirectory();
            dir = dir.startsWith("/") ? dir.replaceFirst("/", "") : dir;
            dir = FileHelper.getParentDirectoryFromFile(new File(dir));
            String mainStoragePath = FileHelper.getInternalMainStorageDirectory(this).getPath();
            if (!photographerConfig.isUsingInternalStorage() &&
                    FileHelper.isExternalStorageMounted() &&
                    !FileHelper.isExternalStorageReadOnly()) {
                mainStoragePath = FileHelper.getExternalMainStorageDirectory(this).getPath();
            }
            if (!photographerConfig.getDirectory().contains(mainStoragePath)) {
                path = mainStoragePath + File.separator + dir;
            } else {
                path = dir;
            }
        } else {
            path = FileHelper.getInternalStorageDirectory(this).getPath();
            if (!photographerConfig.isUsingInternalStorage() &&
                    FileHelper.isExternalStorageMounted() &&
                    !FileHelper.isExternalStorageReadOnly()) {
                path = FileHelper.getExternalStorageDirectory(this).getPath();
            }
        }
        fileTree = new File(path);
        if (!fileTree.exists()) {
            if (!fileTree.mkdirs()) {
                showFileWarningDialog();
                return false;
            }
        } else if (!fileTree.canWrite()) {
            showFileWarningDialog();
            return false;
        }
        return true;
    }

    private void launchCamera() {
        try {
            initCamera();
        } catch (IOException e) {
            e.printStackTrace();
            closePhotoghrapher(RESULT_CANCELED);
        }
    }

    private void closePhotoghrapher(int resultCode) {
        Intent data = new Intent();
        if (resultCode == RESULT_OK) {
            data.putExtra("data", images);
            setResult(RESULT_OK, data);
        } else {
            setResult(resultCode);
        }
        finish();
    }

    private void initCamera() throws IOException {
        String fileName = "";
        if (photographerConfig.isSingle()) {
            if (TextUtils.isEmpty(photographerConfig.getFileName())) {
                fileName = "Photo_" + Calendar.getInstance().getTimeInMillis();
            } else {
                fileName = photographerConfig.getFileName().replace(" ", "_");
            }
        } else {
            fileName = "Photo_" + Calendar.getInstance().getTimeInMillis();
        }
        imageFile = FileHelper.createNewEmptyFile(fileTree.getPath(), fileName, ".jpg");
        if (imageFile != null) {
            startCamera();
        } else {
            throw new IOException("No se puede crear un archivo en el directorio.(Permiso denegado)");
        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoUri = FileProvider.getUriForFile(this, "cl.ceisufro.filehelper.fileprovider", imageFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        startActivityForResult(intent, REQUEST_CAMERA_LAUNCH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA_LAUNCH) {
            if (resultCode == RESULT_OK) {
                if(imageFile!=null){
                    if (photographerConfig.isSingle()) {
                        images.add(new Image(imageFile.getPath(), imageFile.getName()));
                        closePhotoghrapher(RESULT_OK);
                    } else {
                        images.add(new Image(imageFile.getPath(), imageFile.getName()));
                        imageFile = null;
                    }
                }
            } else {
                if (imageFile != null) {
                    imageFile.delete();
                }
                if (!images.isEmpty()) {
                    closePhotoghrapher(RESULT_OK);
                } else {
                    closePhotoghrapher(RESULT_CANCELED);
                }
            }
        } else if (requestCode == REQUEST_CREATE_FILE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                String path = UriUtils.getPath(this, uri);
                imageFile = new File(path);
                startCamera();
            } else {
                if (!images.isEmpty()) {
                    closePhotoghrapher(RESULT_OK);
                } else {
                    closePhotoghrapher(RESULT_CANCELED);
                }
            }
        }
    }

    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissionsList = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.CAMERA);
            }
            if (permissionsList.size() > 0) {
                String permissions[] = permissionsList.toArray(new String[0]);
                requestPermissions(permissions, REQUEST_CAMERA_PERMISSIONS);
            }
        }
    }

    private boolean shouldRequestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissionsList = new ArrayList<>();
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.CAMERA);
            }
            return permissionsList.size() > 0;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSIONS) {
            if (grantResults.length == 0) {
                Toast.makeText(this, getString(R.string.permisos_almacenamiento_necesarios), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                boolean granted = true;
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        granted = false;
                    }
                }
                if (granted) {
                    recreate();
                } else {
                    Toast.makeText(this, getString(R.string.permisos_almacenamiento_necesarios), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    }

    private void showFileWarningDialog() {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.show(getFragmentManager(), "Confirmar");
    }


    @Override
    public void onPositiveClicked() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.setType("image/jpeg");
        String title = "";
        if (!TextUtils.isEmpty(photographerConfig.getFileName())) {
            title = photographerConfig.getFileName();
            title = title.replaceAll(" ", "_");
            title += ".jpg";
        } else {
            title = "Photo" + Calendar.getInstance().getTimeInMillis() + ".jpg";
        }
        intent.putExtra(Intent.EXTRA_TITLE, title);
        startActivityForResult(intent, REQUEST_CREATE_FILE);
    }

    @Override
    public void onNegativeClicked() {
        finish();
    }
}
