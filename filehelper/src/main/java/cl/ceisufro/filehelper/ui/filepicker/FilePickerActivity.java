package cl.ceisufro.filehelper.ui.filepicker;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import cl.ceisufro.filehelper.R;
import cl.ceisufro.filehelper.core.FileHelper;
import cl.ceisufro.filehelper.core.MimeType;

public class FilePickerActivity extends AppCompatActivity {
    private final int REQUEST_STORAGE_PERMISSIONS = 501;
    private GridView filesGridView;
    private GridViewAdapter filesGridViewAdapter;
    private FilePickerConfig filePickerConfig;
    private ArrayList<FileExtended> selectedFiles = new ArrayList<>();
    private File rootDir;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_picker);
        if (savedInstanceState != null) {
            filePickerConfig = (FilePickerConfig) savedInstanceState.getSerializable("SAVED_FILEPICKERCONFIG");
            rootDir = (File) savedInstanceState.getSerializable("SAVED_ACTUALDIR");
            selectedFiles = (ArrayList<FileExtended>) savedInstanceState.getSerializable("SAVED_SELECTEDFILES");
        } else if (getIntent().hasExtra("FILEPICKERCONFIG")) {
            filePickerConfig = (FilePickerConfig) getIntent().getExtras().getSerializable("FILEPICKERCONFIG");
            if (!TextUtils.isEmpty(filePickerConfig.getPath())) {
                File initialDir = new File(filePickerConfig.getPath());
                if (initialDir.exists()) {
                    if (initialDir.isDirectory()) {
                        rootDir = new File(filePickerConfig.getPath());
                    } else {
                        rootDir = new File(FileHelper.getParentDirectoryFromFile(initialDir));
                    }
                }
            }
        } else {
            finish();
        }
        updateTitleBar();
        filesGridView = findViewById(R.id.file_picker_gridview);
        if (rootDir != null && rootDir.exists()) {
            filesGridViewAdapter = new GridViewAdapter(this, getFilesFromDir(rootDir));
        } else {
            rootDir = new File("/storage");
            filesGridViewAdapter = new GridViewAdapter(this, getFilesFromDir(rootDir));
        }
        filesGridView.setAdapter(filesGridViewAdapter);
        filesGridView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        filesGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FileExtended fileExtended = filesGridViewAdapter.getItem(i);
                if (fileExtended != null) {
                    if (fileExtended.isDirectory()) {
                        String split[] = fileExtended.getPath().split(File.separator);
                        if (split.length > 0 && split[split.length - 1].equals("emulated")) {
                            fileExtended = new FileExtended("/storage/emulated/0");
                        }
                        rootDir = fileExtended;
                        filesGridViewAdapter = new GridViewAdapter(FilePickerActivity.this, getFilesFromDir(rootDir));
                        filesGridView.setAdapter(filesGridViewAdapter);
                        updateMenuItems();
                    } else {
                        if (!selectedFiles.contains(fileExtended)) {
                            if (filePickerConfig.getMaxFiles() == 0 || selectedFiles.size() < filePickerConfig.getMaxFiles()) {
                                fileExtended.setSelected(true);
                                selectedFiles.add(fileExtended);
                                filesGridViewAdapter.notifyDataSetChanged();
                                if (filePickerConfig.isSingle()) {
                                    closeFilePicker();
                                }
                                updateMenuItems();
                            }
                        } else {
                            fileExtended.setSelected(false);
                            selectedFiles.remove(fileExtended);
                            filesGridViewAdapter.notifyDataSetChanged();
                            updateMenuItems();
                        }
                        updateTitleBar();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (filePickerConfig.isRequestingPermission()) {
            requestPermissions();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.file_picker_menu, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                filesGridViewAdapter.filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filesGridViewAdapter.filter(s);
                return true;
            }
        });
        this.menu = menu;
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("SAVED_FILEPICKERCONFIG", filePickerConfig);
        outState.putSerializable("SAVED_ACTUALDIR", rootDir);
        outState.putSerializable("SAVED_SELECTEDFILES", selectedFiles);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_finish) {
            closeFilePicker();
        }
        if (id == R.id.action_select_content) {
            ArrayList<FileExtended> files = filesGridViewAdapter.getItems();
            if (!folderHasSelectedItems(rootDir)) {
                for (FileExtended fileExtended : files) {
                    if (!fileExtended.isPreviousDir() && !fileExtended.isDirectory()) {
                        if (filePickerConfig.getMaxFiles() == 0 || selectedFiles.size() < filePickerConfig.getMaxFiles()) {
                            fileExtended.setSelected(true);
                            selectedFiles.add(fileExtended);
                            filesGridViewAdapter.notifyDataSetChanged();
                            if (filePickerConfig.isSingle()) {
                                closeFilePicker();
                            }
                        } else {
                            break;
                        }
                    }
                }
            } else {
                for (FileExtended fileExtended : files) {
                    fileExtended.setSelected(false);
                    selectedFiles.remove(fileExtended);
                    filesGridViewAdapter.notifyDataSetChanged();

                }
            }
            updateMenuItems();
            updateTitleBar();
        }
        if (id == R.id.action_unselect_all) {
            selectedFiles.clear();
            updateMenuItems();
            updateTitleBar();
            filesGridViewAdapter = new GridViewAdapter(this, getFilesFromDir(rootDir));
            filesGridView.setAdapter(filesGridViewAdapter);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Cierra el Selector de Archivos y retorna el resultado a la actividad sobre la cual
     * se haya llamado esta Activity.
     */
    private void closeFilePicker() {
        Intent intent = new Intent();
        intent.putExtra("data", getSelectedFiles());
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Obtiene la lista de archivos del directorio que se ingrese como parámetro.
     *
     * @param rootDir {@link File} - Directorio para extraer el listado de archivos.
     * @return {@link ArrayList}<{@link FileExtended}> - Listado de archivos o una lista
     * vacía si el directorio no contenia ningun archivo.
     */
    private ArrayList<FileExtended> getFilesFromDir(File rootDir) {
        ArrayList<FileExtended> files = new ArrayList<>();
        File previousDir = getPreviousDir(rootDir);
        if (previousDir != null) {
            files.add(new FileExtended(previousDir, false, true));
        }
        if (rootDir.listFiles() != null) {
            ArrayList<File> fileList = new ArrayList<>();
            for (File file : Arrays.asList(rootDir.listFiles())) {
                if (file.isDirectory() || isFileValid(file)) {
                    fileList.add(file);
                }
            }
            Collections.sort(fileList, new Comparator<File>() {
                @Override
                public int compare(File file1, File file2) {
                    return file1.getName().compareToIgnoreCase(file2.getName());
                }
            });
            for (File file : fileList) {
                boolean added = false;
                for (FileExtended fileExtended : selectedFiles) {
                    if (file.getPath().equals(fileExtended.getPath())) {
                        files.add(fileExtended);
                        added = true;
                    }
                }
                if (!added) {
                    files.add(new FileExtended(file));
                }
            }
        }
        return files;
    }

    /**
     * Retorna el directorio previo de algún archivo o directorio. El directorio previo
     * se le considera a cualquier carpeta que este previa al directorio o archivo consultado y
     * que cuente con permisos de lectura.
     *
     * @param dir {@link File} - Archivo o directorio para buscar su directorio previo.
     * @return {@link File} - Directorio previo o null si no existe alguno o no permite lectura.
     */
    private File getPreviousDir(File dir) {
        try {
            File previousDir;
            String path = dir.getParent();
            if (!TextUtils.isEmpty(path)) {
                previousDir = new File(path);
                if (!previousDir.getPath().equals(File.separator)) {
                    String split[] = previousDir.getPath().split(File.separator);
                    if (split.length > 0 && split[split.length - 1].equals("emulated")) {
                        previousDir = new File("/storage");
                    }
                    if (previousDir.canRead()) {
                        return previousDir;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean folderHasSelectedItems(File file) {
        ArrayList<FileExtended> fileList = getFilesFromDir(file);
        if (fileList != null && !fileList.isEmpty()) {
            for (FileExtended fileExtended : fileList) {
                if (fileExtended.isSelected()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Actualiza el titulo actual de la AppBar.
     */
    private void updateTitleBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            String title;
            if (!TextUtils.isEmpty(filePickerConfig.getActionBarTitle())) {
                title = filePickerConfig.getActionBarTitle();
            } else {
                title = getString(R.string.action_bar_title);
            }
            if (!selectedFiles.isEmpty()) {
                if (filePickerConfig.getMaxFiles() > 0) {
                    title += " " + selectedFiles.size() + "/" + filePickerConfig.getMaxFiles();
                } else {
                    title += " " + selectedFiles.size();
                }
            }
            actionBar.setTitle(title);
        }
    }

    private void updateMenuItems() {
        MenuItem selectFolder = menu.getItem(2);
        MenuItem unselectAll = menu.getItem(3);
        if (folderHasSelectedItems(rootDir)) {
            selectFolder.setIcon(R.drawable.ic_select_off);
            selectFolder.setTitle(R.string.unselect_folder);
        } else {
            selectFolder.setIcon(R.drawable.ic_select_all);
            selectFolder.setTitle(R.string.select_all);
        }
        if (selectedFiles.isEmpty()) {
            unselectAll.setVisible(false);
        } else {
            unselectAll.setVisible(true);
        }
    }

    /**
     * Comprueba que el archivo actual es valido para mostrar segun los filtros indicados por
     * {@link FilePicker.Builder#addMimeSubType(String)}, {@link FilePicker.Builder#addMimeType(String)},
     * {@link FilePicker.Builder#addMimeSubType(String...)} o {@link FilePicker.Builder#addMimeType(String...)}.
     * La valides esta determinada según la siguiente lógica, si el archivo esta dentro de la lista de tipos,
     * si el archivo calza con el tipo y subtipo y por ultimo, si el archivo calza con el subtipo y no hay
     * filtro de tipo o no calza con ninguno.
     * EJ:
     * <p>
     * *Si de filtro de tipo se ingresó "image" y de subtipo se ingresó "jpeg" y "mp3". Los resultados
     * serán todos los archivos que sean imágenes de subtipo jpeg y además archivos de audio mp3.
     * <p>
     * *Si de filtro de tipo se ingresó "image" y "audio" y de subtipo se ingresó "mp3". Los resultados
     * serán todos los archivos que sean imágenes y además archivos de audio mp3.
     *
     * @param file {@link File} - Archivo al cual se examinara su valides.
     * @return boolean - true si el archivo es valido según los filtros especificados; false si no lo es valido.
     */
    private boolean isFileValid(File file) {
        ArrayList<String> mimeTypes = filePickerConfig.getMimeTypes();
        ArrayList<String> mimeSubTypes = filePickerConfig.getMimeSubTypes();
        MimeType mimeType = FileHelper.getMimeTypeFromFile(file);
        if (mimeType != null) {
            boolean matchType = false;
            boolean matchSubType = false;
            for (String type : mimeTypes) {
                if (!TextUtils.isEmpty(type) &&
                        !TextUtils.isEmpty(mimeType.getType()) &&
                        mimeType.getType().equals(type)) {
                    matchType = true;
                }
            }
            for (String subType : mimeSubTypes) {
                if (!TextUtils.isEmpty(subType) &&
                        !TextUtils.isEmpty(mimeType.getSubtype()) &&
                        mimeType.getSubtype().equals(subType)) {
                    matchSubType = true;
                }
            }
            return matchType && matchSubType ||
                    matchType ||
                    matchSubType && (mimeTypes.size() == 0 || !mimeTypes.contains(mimeType.getType()));
        } else {
            return false;
        }
    }

    /**
     * Retorna la lista de archivo seleccionados en forma de {@link ArrayList}<{@link File}>.
     *
     * @return {@link ArrayList}<{@link File}> - Lista de archivos seleccionados.
     */
    private ArrayList<File> getSelectedFiles() {
        ArrayList<File> files = new ArrayList<>();
        for (FileExtended fileExtended : selectedFiles) {
            files.add(new File(fileExtended.getPath()));
        }
        return files;
    }

    /**
     * Pide permisos de lectura para la aplicación.
     */
    private void requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                String permissions[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permissions, REQUEST_STORAGE_PERMISSIONS);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE_PERMISSIONS) {
            if (grantResults.length < 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getString(R.string.permisos_almacenamiento_necesarios), Toast.LENGTH_SHORT).show();
                finish();
            } else {
                recreate();
            }
        }
    }
}
