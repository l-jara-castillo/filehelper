package cl.ceisufro.filehelper.ui.photographer;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Luis Andrés Jara Castillo on 11-10-17.
 */

class PhotographerConfig implements Serializable {
    private boolean single;
    private boolean internalStorage;
    private boolean requestPermissions;
    private int maxPhotos;
    private String directoryPath;
    private String fileName;

    PhotographerConfig(Photographer photographer) {
        this.single = photographer.isSingle();
        this.internalStorage = photographer.isUsingInternalStorage();
        this.requestPermissions = photographer.isRequestingPermissions();
        this.maxPhotos = photographer.getMaxPhotos();
        this.directoryPath = photographer.getDirectory();
        this.fileName = photographer.getFileName();
    }

    boolean isSingle() {
        return single;
    }

    boolean isUsingInternalStorage() {
        return internalStorage;
    }

    boolean isRequestingPermissions() {
        return requestPermissions;
    }

    int getMaxPhotos() {
        return maxPhotos;
    }

    String getDirectory() {
        return directoryPath;
    }

    String getFileName() {
        return fileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhotographerConfig)) return false;
        PhotographerConfig that = (PhotographerConfig) o;
        return isSingle() == that.isSingle() &&
                isUsingInternalStorage() == that.isUsingInternalStorage() &&
                isRequestingPermissions() == that.isRequestingPermissions() &&
                getMaxPhotos() == that.getMaxPhotos() &&
                Objects.equals(getDirectory(), that.getDirectory()) &&
                Objects.equals(getFileName(), that.getFileName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSingle(), isUsingInternalStorage(), isRequestingPermissions(), getMaxPhotos(), getDirectory());
    }

    @Override
    public String toString() {
        return "PhotographerConfig{" +
                "setSingleRecordingMode=" + single +
                "internalStorage=" + internalStorage +
                "requestPermission=" + requestPermissions +
                ", maxPhotos=" + maxPhotos +
                ", directoryPath='" + directoryPath + '\'' +
                '}';
    }
}
